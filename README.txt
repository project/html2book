HTML2BOOK README.txt
====================


CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Similar projects
* Maintainers


INTRODUCTION
------------

The HTML2Book module allows you to create multi-page books by
splitting the body text of a book page node into multiple nodes upon
save.  The split points are based upon HTML heading tags.

This makes it possible to create a book in a word processing program,
export the result to HTML, and then use this HTML to create a
multipage Drupal book.


REQUIREMENTS
------------

* Advanced help hint[1];
  Required to link help text to online help and advanced help.


RECOMMENDED MODULES
-------------------

* Advanced help[2]
  When this module is enabled, you will have access to extended online help.


INSTALLATION
------------

1. Install as you would normally install a contributed drupal
   module. See: “Installing modules“[3] for further information.

2. Enable the HTML2Body module on the Modules list page.

3. Navigate to the Admin » People » Permissions page and give the
   roles you will grant access to create multi-page books by means of
   this module the permission “Use HTML2Book“.


CONFIGURATION
-------------

The module has no menu or modifiable settings.  There is no global
configuration.  When enabled, the module will add the field group
named “HTML2Book Splitter” below the book page “Body” field.  To
remove this field group, disable the module and clear caches.

The books pages created with this module are regular book pages that
will stay around after the module has been disabled.  On a production
site, once you're done converting the HTML files you want to convert,
you should disable the module.


SIMILAR PROJECTS
----------------

I am aware of two other projects that are similar to this:

* HTML Import[4]:
  This is a project that started in 2015 and creates a Drupal book
  from HTML. It seems to specifically address conversion of HTML
  exported from MS Word, Adobe Indesign and PDF.

* Import HTML[5]:
  This project converts pages linked from a siteroot on a legacy web
  server into Drupal nodes, retaining the original link structure.


MAINTAINERS
-----------

* KarenS - https://www.drupal.org/user/45874 (original author)
* gisle - https://www.drupal.org/u/gisle (current maintainer)


Any help with development (patches, reviews, comments) are welcome.

[1] https://www.drupal.org/project/advanced_help_hint
[2] https://www.drupal.org/project/advanced_help
[3] https://www.drupal.org/documentation/install/modules-themes/modules-7
[4] https://www.drupal.org/project/html_import
[5] https://www.drupal.org/project/import_html
